import { ref, onMounted, computed } from "vue";
import { useRoute, useRouter } from "vue-router";
import TransactionTable from "../components/TransactionTable";
import { initializeApp } from "firebase/app";
import {
  getAuth
} from "firebase/auth"; // Import EmailAuthProvider
import {
  getFirestore,
  query,
  collection,
  where,
  getDocs
} from "firebase/firestore";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp); // Initialize Cloud Firestore and get a reference to the service

export default {
  components: { TransactionTable },
  setup() {
    // Define reactive variables
    const router = useRouter();
    const route = useRoute();
    let MoaDocumentDetails = ref([]);
    const DocumentCount = ref(0);
    const OngoingCount = ref(0);
    const PendingCount = ref(0);
    const CompletedCount = ref(0);

    // Fetch transaction details based on trNo
    const fetchAllTransaction = async () => {
      try {
        const q = query(
          collection(db, "transactions")
        );
        const querySnapshot = await getDocs(q);
        const transactions = [];
        querySnapshot.forEach((doc) => {
          transactions.push({ id: doc.id, ...doc.data() });
        });
        return transactions;
      } catch (error) {
        console.error("Error fetching Transaction:", error);
        return [];
      }
    };

    const fetchAllFiles = async () => {
      try {
        const q = query(
          collection(db, "files")
        );
        const querySnapshot = await getDocs(q);
        const files = [];
        querySnapshot.forEach((doc) => {
          files.push({ id: doc.id, ...doc.data() });
        });
        return files;
      } catch (error) {
        console.error("Error fetching Transaction:", error);
        return [];
      }
    };

    // Fetch MoaDocumentDetails and update trStep
    const fetchMoa = async () => {
      try {
        const data = await fetchAllTransaction();
        const filteredDocuments = data.filter((doc) => doc.trStep >= 2);
        MoaDocumentDetails.value = filteredDocuments;
        const notarizedDocuments = await fetchAllFiles();
        DocumentCount.value = notarizedDocuments.length;
        const ongoingDocuments = data.filter((doc) => doc.trStep >= 3 && doc.trStep <= 5);
        OngoingCount.value = ongoingDocuments.length;
        const pendingDocuments = data.filter((doc) => doc.trStep == 2);
        PendingCount.value = pendingDocuments.length;
        const completedDocuments = data.filter((doc) => doc.trStep == 6);
        CompletedCount.value = completedDocuments.length;
      } catch (error) {
        console.error("Error fetching MoA details:", error);
      }
    };

    onMounted(() => {
      fetchMoa();
    });

    
    return {
      DocumentCount,
      OngoingCount,
      PendingCount,
      CompletedCount,
      MoaDocumentDetails
    };
  },
  data() {
    return {
      visible: true,
    };
  },
  mounted() {
    setTimeout(() => {
      this.visible = false;
    }, 500);
  },
};

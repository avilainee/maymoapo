import { ref, onMounted, computed } from "vue";
import { exportFile } from "quasar";
import { initializeApp } from "firebase/app";
import {
  getFirestore,
  query,
  collection,
  where,
  getDocs
} from "firebase/firestore";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp); // Initialize Cloud Firestore and get a reference to the service

export default {
  setup() {
    // Define reactive variables
    let MoaDocumentDetails = ref([]);
    let filter = ref("");
    let notaryDateFilter = ref("All time");
    let customStartDate = ref(null);
    let customEndDate = ref(null);

    const notaryDateOptions = [
      "All time",
      "Today",
      "Yesterday",
      "Past 7 days",
      "Past 30 days",
      "Custom Range",
    ];

    // Fetch transaction details based on trNo
    const fetchAllTransaction = async () => {
      try {
        const q = query(
          collection(db, "files")
        );
        const querySnapshot = await getDocs(q);
        const files = [];
        querySnapshot.forEach((doc) => {
          files.push({ id: doc.id, ...doc.data() });
        });
        return files;
      } catch (error) {
        console.error("Error fetching Transaction:", error);
        return [];
      }
    };

    // Fetch MoaDocumentDetails and update trStep
    const fetchMoa = async () => {
      try {
        const data = await fetchAllTransaction();
        const filteredDocuments = data.filter(
          (doc) => doc.notaryDate !== null
        );
        MoaDocumentDetails.value = filteredDocuments;
      } catch (error) {
        console.error("Error fetching MoA details:", error);
      }
    };

    onMounted(() => {
      fetchMoa();
    });

    const initialPagination = {
      rowsPerPage: 15,
    };

    // Define columns
    const columns = [
      {
        name: "index",
        label: "#",
        field: "id", // Use the function to get the incremented index
      },
      {
        name: "hteName",
        required: true,
        label: "HTE/Company Name",
        align: "left",
        field: "hteName",
        sortable: true,
      },
      {
        required: true,
        label: "Address",
        align: "left",
        format: (val, row) => `${row.barangay}, ${row.city}, ${row.province}`,
        sortable: true,
      },
      {
        name: "hteEmail",
        required: true,
        label: "Email",
        align: "left",
        field: "hteEmail",
      },
      {
        name: "notaryDate",
        required: true,
        label: "Notary",
        align: "left",
        field: "notaryDate",
        sortable: true,
      },
      {
				name: 'actions',
				align: 'right',
				field: 'actions',
			},
    ];

    // Function to wrap CSV value
    function wrapCsvValue(val, formatFn, row) {
      let formatted = formatFn !== void 0 ? formatFn(val, row) : val;
      formatted =
        formatted === void 0 || formatted === null ? "" : String(formatted);
      formatted = `"${formatted.replace(/"/g, '""')}"`;
      return formatted;
    }

    // Computed property for filtered MoaDocumentDetails
    const filteredMoaDocumentDetails = computed(() => {
      // Filter by notary date
      if (notaryDateFilter.value === "Today") {
        // Filter documents with notary date equal to today's date
        const today = new Date();
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          return (
            notaryDate.getFullYear() === today.getFullYear() &&
            notaryDate.getMonth() === today.getMonth() &&
            notaryDate.getDate() === today.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Yesterday") {
        // Filter documents with notary date equal to yesterday's date
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1); // Set date to yesterday
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          return (
            notaryDate.getFullYear() === yesterday.getFullYear() &&
            notaryDate.getMonth() === yesterday.getMonth() &&
            notaryDate.getDate() === yesterday.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Past 7 days") {
        // Filter documents with notary date within the past 7 days
        const today = new Date();
        const sevenDaysAgo = new Date(today);
        sevenDaysAgo.setDate(today.getDate() - 7); // Set date to 7 days ago
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          return notaryDate >= sevenDaysAgo && notaryDate <= today;
        });
      } else if (notaryDateFilter.value === "Past 30 days") {
        // Filter documents with notary date within the past 30 days
        const today = new Date();
        const thirtyDaysAgo = new Date(today);
        thirtyDaysAgo.setDate(today.getDate() - 30);
        console.log(thirtyDaysAgo);
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          return notaryDate >= thirtyDaysAgo && notaryDate <= today;
        });
      } else if (customStartDate.value == null && customEndDate.value == null) {
        notaryDateFilter.value === "Custom Range";
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return MoaDocumentDetails.value;
        });
      } else if (customStartDate.value != null && customEndDate.value == null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return notaryDate >= startDay;
        });
      } else if (customStartDate.value == null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return notaryDate <= endDay;
        });
      } else if (customStartDate.value != null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return notaryDate >= startDay && notaryDate <= endDay;
        });
      } else {
        return MoaDocumentDetails.value; // Return all documents if no specific filter is selected
      }
    });

    // Function to export table to CSV
    const exportTable = () => {
      // Generate CSV content
      const content = [columns.map((col) => wrapCsvValue(col.label))]
        .concat(
          filteredMoaDocumentDetails.value.map((row) =>
            columns
              .map((col) =>
                wrapCsvValue(
                  typeof col.field === "function"
                    ? col.field(row)
                    : row[col.field === void 0 ? col.name : col.field],
                  col.format,
                  row
                )
              )
              .join(",")
          )
        )
        .join("\r\n");

      // Initiate download using Quasar's exportFile
      exportFile("existing moa_table-export.csv", content, "text/csv");
    };

  const onPreviewMoa = async (row) => {
      window.open(row.hteMoa, '_blank');
  };

    return {
      initialPagination,
      customStartDate,
      customEndDate,
      filter,
      MoaDocumentDetails,
      columns,
      notaryDateOptions,
      notaryDateFilter,
      filteredMoaDocumentDetails,
      exportTable,
      onPreviewMoa,
    };
  },
  data() {
    return {
      visible: true,
    };
  },
  mounted() {
    setTimeout(() => {
      this.visible = false;
    }, 500);
  },
};

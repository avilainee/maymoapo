import { ref, onMounted } from 'vue';
import { useRouter } from "vue-router";
import { useQuasar } from "quasar";
import { initializeApp } from "firebase/app"; 
import { getAuth, signOut, reauthenticateWithCredential, updatePassword, EmailAuthProvider } from 'firebase/auth'; // Import EmailAuthProvider
import { getFirestore, collection, query, where, getDocs } from "firebase/firestore";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(firebaseApp);

export default {
  setup() {
    const AccountDetails = ref([]);
    const form = ref({
      name: "",
      role: "",
      email: "",
    });
    const router = useRouter();
    const $q = useQuasar();
    let opassword = ref("");
    let npassword = ref("");
    let cpassword = ref("");

    const fetchAccountByEmail = async (email) => {
      try {
        console.log("Fetching account for email:", email);
        const q = query(collection(db, "accounts"), where("email", "==", email));
        const querySnapshot = await getDocs(q);
        const accounts = [];
        querySnapshot.forEach((doc) => {
          accounts.push({ id: doc.id, ...doc.data() });
        });
        console.log("Account by email:", accounts);
        return accounts;
      } catch (error) {
        console.error("Error fetching account by email:", error);
        return [];
      }
    };

    onMounted(async () => {
      const auth = getAuth(firebaseApp);
      const user = auth.currentUser;

      if (user) {
        form.value.name = user.displayName;
        form.value.email = user.email;
        const data = await fetchAccountByEmail(form.value.email);
        AccountDetails.value = data;
        form.value.role = AccountDetails.value[0].role;
      }
    });

    const onConfirm = async () => {
      try {
        const auth = getAuth(firebaseApp);
        const user = auth.currentUser;
        
        // Reauthenticate user before changing password
        const credential = EmailAuthProvider.credential(
          user.email,
          opassword.value
        );
        await reauthenticateWithCredential(user, credential);
        
        // Update the user's password
        await updatePassword(user, npassword.value);
        
        $q.notify({
          color: "positive",
          position: "top",
          message: "Password updated successfully.",
          icon: "check",
        });
        
        // Clear the password fields after successful update
        opassword.value = "";
        npassword.value = "";
        cpassword.value = "";
      } catch (error) {
        console.error("Error changing password:", error);
        $q.notify({
          color: "negative",
          position: "top",
          message: "Error updating password.",
          icon: "report_problem",
        });
      }
    };

    const onReset = async () => {
      opassword.value = "";
      npassword.value = "";
      cpassword.value = "";
    }

    return {
      onConfirm,
      form,
      opassword,
      npassword,
      cpassword,
      isoPwd: ref(true),
      isPwd: ref(true),
      iscPwd: ref(true),
      onReset
    };
  },
  computed: {
    passwordsMatch() {
      return this.form.password === this.form.cpassword;
    },
    showMismatchError() {
      return this.form.password && this.form.cpassword && !this.passwordsMatch;
    },
  },
  data() {
    return {
      visible: true,
    };
  },
  mounted() {
    setTimeout(() => {
      this.visible = false;
    }, 500);
  },
};

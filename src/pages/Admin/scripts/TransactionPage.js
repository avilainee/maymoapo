import { ref, computed, onMounted, watch } from "vue";
import { useRoute, useRouter } from "vue-router";
import { useQuasar, date } from "quasar";
import TransactionDetails from "../components/TransactionDetails";
import TransactionTable from "../components/TransactionTable";
import { initializeApp } from "firebase/app";
import {
  getAuth
} from "firebase/auth"; // Import EmailAuthProvider
import {
  getFirestore,
  query,
  collection,
  where,
  getDocs
} from "firebase/firestore";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp); // Initialize Cloud Firestore and get a reference to the service

export default {
  components: { TransactionDetails, TransactionTable },
  setup() {
    const router = useRouter();
    const route = useRoute();
    let MoaDocumentDetails = ref([]);
    let OngoingMoaDocument = ref([]);
    let PendingMoaDocument = ref([]);
    let CompletedMoaDocument = ref([]);
    const panel = ref("All Transactions");
    const trNo = ref(route.query.trNo || null); // Initialize trNo from route query
    const search = ref("");
    let notaryDateFilter = ref("All time");
    let customStartDate = ref(null);
    let customEndDate = ref(null);

    const notaryDateOptions = [
      "All time",
      "Today",
      "Yesterday",
      "Past 7 days",
      "Past 30 days",
      "Custom Range",
    ];

    const handleItemClick = (trNo) => {
      router.push({
        path: "transaction",
        query: {
          trNo: trNo,
        },
      });
    };

    // Watch for route changes to update trNo
    watch(route, (newRoute) => {
      trNo.value = newRoute.query.trNo || null;
    });

    // Fetch transaction details based on trNo
    const fetchAllTransaction = async () => {
      try {
        const q = query(
          collection(db, "transactions")
        );
        const querySnapshot = await getDocs(q);
        const transactions = [];
        querySnapshot.forEach((doc) => {
          transactions.push({ id: doc.id, ...doc.data() });
        });
        return transactions;
      } catch (error) {
        console.error("Error fetching Transaction:", error);
        return [];
      }
    };

    // Fetch MoaDocumentDetails and update trStep
    const fetchMoa = async () => {
      try {
        const data = await fetchAllTransaction();
        const filteredDocuments = data.filter((doc) => doc.trStep >= 2);
        MoaDocumentDetails.value = filteredDocuments;
        const ongoingDocuments = data.filter(
          (doc) => doc.trStep >= 3 && doc.trStep <= 5
        );
        OngoingMoaDocument.value = ongoingDocuments;
        const pendingDocuments = data.filter((doc) => doc.trStep == 2);
        PendingMoaDocument.value = pendingDocuments;
        const completedDocuments = data.filter((doc) => doc.trStep == 6);
        CompletedMoaDocument.value = completedDocuments;
      } catch (error) {
        console.error("Error fetching MoA details:", error);
      }
    };

    onMounted(() => {
      fetchMoa();
    });

    watch(() => panel.value, fetchMoa);

    // Computed property for filtered MoaDocumentDetails
    const filteredMoaDocumentDetails = computed(() => {
      if (notaryDateFilter.value === "Today") {
        // Filter documents with notary date equal to today's date
        const today = new Date();
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return (
            updateStepdate.getFullYear() === today.getFullYear() &&
            updateStepdate.getMonth() === today.getMonth() &&
            updateStepdate.getDate() === today.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Yesterday") {
        // Filter documents with notary date equal to yesterday's date
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1); // Set date to yesterday
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return (
            updateStepdate.getFullYear() === yesterday.getFullYear() &&
            updateStepdate.getMonth() === yesterday.getMonth() &&
            updateStepdate.getDate() === yesterday.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Past 7 days") {
        // Filter documents with notary date within the past 7 days
        const today = new Date();
        const sevenDaysAgo = new Date(today);
        sevenDaysAgo.setDate(today.getDate() - 7); // Set date to 7 days ago
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return updateStepdate >= sevenDaysAgo && updateStepdate <= today;
        });
      } else if (notaryDateFilter.value === "Past 30 days") {
        // Filter documents with notary date within the past 30 days
        const today = new Date();
        const thirtyDaysAgo = new Date(today);
        thirtyDaysAgo.setDate(today.getDate() - 30);
        console.log(thirtyDaysAgo);
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return updateStepdate >= thirtyDaysAgo && updateStepdate <= today;
        });
      } else if (customStartDate.value == null && customEndDate.value == null) {
        notaryDateFilter.value === "Custom Range";
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return MoaDocumentDetails.value;
        });
      } else if (customStartDate.value != null && customEndDate.value == null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay;
        });
      } else if (customStartDate.value == null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay;
        });
      } else if (customStartDate.value != null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay && updateStepdate <= endDay;
        });
      } else {
        return MoaDocumentDetails.value; // Return all documents if no specific filter is selected
      }
    });

    const OngoingMoaDocumentDetails = computed(() => {
      MoaDocumentDetails.value = OngoingMoaDocument.value;
      if (notaryDateFilter.value === "Today") {
        // Filter documents with notary date equal to today's date
        const today = new Date();
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return (
            updateStepdate.getFullYear() === today.getFullYear() &&
            updateStepdate.getMonth() === today.getMonth() &&
            updateStepdate.getDate() === today.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Yesterday") {
        // Filter documents with notary date equal to yesterday's date
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1); // Set date to yesterday
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return (
            updateStepdate.getFullYear() === yesterday.getFullYear() &&
            updateStepdate.getMonth() === yesterday.getMonth() &&
            updateStepdate.getDate() === yesterday.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Past 7 days") {
        // Filter documents with notary date within the past 7 days
        const today = new Date();
        const sevenDaysAgo = new Date(today);
        sevenDaysAgo.setDate(today.getDate() - 7); // Set date to 7 days ago
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return updateStepdate >= sevenDaysAgo && updateStepdate <= today;
        });
      } else if (notaryDateFilter.value === "Past 30 days") {
        // Filter documents with notary date within the past 30 days
        const today = new Date();
        const thirtyDaysAgo = new Date(today);
        thirtyDaysAgo.setDate(today.getDate() - 30);
        console.log(thirtyDaysAgo);
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return updateStepdate >= thirtyDaysAgo && updateStepdate <= today;
        });
      } else if (customStartDate.value == null && customEndDate.value == null) {
        notaryDateFilter.value === "Custom Range";
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return MoaDocumentDetails.value;
        });
      } else if (customStartDate.value != null && customEndDate.value == null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay;
        });
      } else if (customStartDate.value == null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay;
        });
      } else if (customStartDate.value != null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay && updateStepdate <= endDay;
        });
      } else {
        return MoaDocumentDetails.value; // Return all documents if no specific filter is selected
      }
    });

    const PendingMoaDocumentDetails = computed(() => {
      MoaDocumentDetails.value = PendingMoaDocument.value;
      if (notaryDateFilter.value === "Today") {
        // Filter documents with notary date equal to today's date
        const today = new Date();
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return (
            updateStepdate.getFullYear() === today.getFullYear() &&
            updateStepdate.getMonth() === today.getMonth() &&
            updateStepdate.getDate() === today.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Yesterday") {
        // Filter documents with notary date equal to yesterday's date
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1); // Set date to yesterday
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return (
            updateStepdate.getFullYear() === yesterday.getFullYear() &&
            updateStepdate.getMonth() === yesterday.getMonth() &&
            updateStepdate.getDate() === yesterday.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Past 7 days") {
        // Filter documents with notary date within the past 7 days
        const today = new Date();
        const sevenDaysAgo = new Date(today);
        sevenDaysAgo.setDate(today.getDate() - 7); // Set date to 7 days ago
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return updateStepdate >= sevenDaysAgo && updateStepdate <= today;
        });
      } else if (notaryDateFilter.value === "Past 30 days") {
        // Filter documents with notary date within the past 30 days
        const today = new Date();
        const thirtyDaysAgo = new Date(today);
        thirtyDaysAgo.setDate(today.getDate() - 30);
        console.log(thirtyDaysAgo);
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return updateStepdate >= thirtyDaysAgo && updateStepdate <= today;
        });
      } else if (customStartDate.value == null && customEndDate.value == null) {
        notaryDateFilter.value === "Custom Range";
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return MoaDocumentDetails.value;
        });
      } else if (customStartDate.value != null && customEndDate.value == null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay;
        });
      } else if (customStartDate.value == null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay;
        });
      } else if (customStartDate.value != null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay && updateStepdate <= endDay;
        });
      } else {
        return MoaDocumentDetails.value; // Return all documents if no specific filter is selected
      }
    });

    const CompletedMoaDocumentDetails = computed(() => {
      MoaDocumentDetails.value = CompletedMoaDocument.value;
      if (notaryDateFilter.value === "Today") {
        // Filter documents with notary date equal to today's date
        const today = new Date();
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return (
            updateStepdate.getFullYear() === today.getFullYear() &&
            updateStepdate.getMonth() === today.getMonth() &&
            updateStepdate.getDate() === today.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Yesterday") {
        // Filter documents with notary date equal to yesterday's date
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1); // Set date to yesterday
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return (
            updateStepdate.getFullYear() === yesterday.getFullYear() &&
            updateStepdate.getMonth() === yesterday.getMonth() &&
            updateStepdate.getDate() === yesterday.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Past 7 days") {
        // Filter documents with notary date within the past 7 days
        const today = new Date();
        const sevenDaysAgo = new Date(today);
        sevenDaysAgo.setDate(today.getDate() - 7); // Set date to 7 days ago
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return updateStepdate >= sevenDaysAgo && updateStepdate <= today;
        });
      } else if (notaryDateFilter.value === "Past 30 days") {
        // Filter documents with notary date within the past 30 days
        const today = new Date();
        const thirtyDaysAgo = new Date(today);
        thirtyDaysAgo.setDate(today.getDate() - 30);
        console.log(thirtyDaysAgo);
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          return updateStepdate >= thirtyDaysAgo && updateStepdate <= today;
        });
      } else if (customStartDate.value == null && customEndDate.value == null) {
        notaryDateFilter.value === "Custom Range";
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return MoaDocumentDetails.value;
        });
      } else if (customStartDate.value != null && customEndDate.value == null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay;
        });
      } else if (customStartDate.value == null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay;
        });
      } else if (customStartDate.value != null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const updateStepdate = new Date(doc.updateStepdate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return updateStepdate >= startDay && updateStepdate <= endDay;
        });
      } else {
        return MoaDocumentDetails.value; // Return all documents if no specific filter is selected
      }
    });

    const getTimeDifference = (updateSteptime, updateStepdate) => {
      const now = new Date();
      const combinedDateTime = `${updateStepdate} ${updateSteptime}`;
      const updateTime = new Date(combinedDateTime);
      const diffInMs = now - updateTime;
      const diffInMinutes = Math.floor(diffInMs / 60000);

      if (diffInMinutes < 1) {
        return "Just now";
      } else if (diffInMinutes < 60) {
        return `${diffInMinutes} min ago`;
      } else if (diffInMinutes < 1440) {
        const diffInHours = Math.floor(diffInMinutes / 60);
        return `${diffInHours} hr ago`;
      } else {
        const diffInDays = Math.floor(diffInMinutes / 1440);
        return `${diffInDays} day${diffInDays > 1 ? "s" : ""} ago`;
      }
    };

    const getStepBadge = (trStep) => {
      if (trStep == 2) {
        return "For Approval";
      } else if (trStep == 3) {
        return `For ULCO & VPAA`;
      } else if (trStep == 4) {
        return `For Retrieval`;
      } else if (trStep == 5) {
        return `MoA Retrieved`;
      } else {
        return `MoA Uploaded`;
      }
    };
    const getStepColor = (trStep) => {
      switch (trStep) {
        case 2:
          return "primary"; // For Approval
        case 3:
          return "orange"; // For ULCO & VPAA
        case 4:
          return "warning"; // For Retrieval
        case 5:
          return "teal"; // MoA Retrieved
        default:
          return "blue"; // MoA Uploaded
      }
    };

    return {
      search,
      panel,
      getTimeDifference,
      getStepBadge,
      getStepColor,
      handleItemClick,
      MoaDocumentDetails,
      filteredMoaDocumentDetails,
      OngoingMoaDocumentDetails,
      PendingMoaDocumentDetails,
      CompletedMoaDocumentDetails,
      trNo,

      customStartDate,
      customEndDate,
      notaryDateOptions,
      notaryDateFilter,
    };
  },
};

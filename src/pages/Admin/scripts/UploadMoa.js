import { ref, onMounted, computed, watch } from "vue";
import axios from "axios";
import { useQuasar } from "quasar";
import barangays from "../../Servers/barangays.json";
import cities from "../../Servers/cities.json";
import provinces from "../../Servers/provinces.json";
import { useRouter, useRoute } from "vue-router";
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth"; // Import EmailAuthProvider
import {
  getFirestore,
  query,
  collection,
  where,
  getDocs,
  addDoc,
} from "firebase/firestore";

import {
  getStorage,
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from "firebase/storage";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp); // Initialize Cloud Firestore and get a reference to the service
const storage = getStorage(firebaseApp);

export default {
  components: {}, // Optional: Define child components here
  setup() {
    let $q = useQuasar(); // Access Quasar utilities
    let router = useRouter();
    const route = useRoute(); // Accessing current route information
    // Define reactive references for moa fields
    let saveBtnLoadingState = ref(false);
    const provincesList = provinces.data;
    const citiesList = cities.data;
    let cityList = ref([]);
    let barangayList = ref([]);
    const id = ref(route.query.id);
    let validitycount = ref("");
    let validitydate = ref("");
    let moa = ref({
      id: null,
      hteName: "",
      signatoryName: "",
      hteEmail: "",
      province: null,
      barangay: null,
      city: null,
      street: "",
      notaryDate: null,
      hteMoa: null,
      validity: null,
    });

    // Define reactive reference to track if moa fields are interacted with
    const isInteracted = ref({
      province: false,
      barangay: false,
      city: false,
    });

    // Function to mark field as interacted
    const clearErrorMessage = (fieldName) => {
      isInteracted.value[fieldName] = true;
    };

    // Sorting provinces list alphabetically
    provincesList.value = provinces.data.sort((a, b) => {
      // Sort the list alphabetical
      const nameA = a.label.toUpperCase();
      const nameB = b.label.toUpperCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });
    // Function to fetch cities and barangays based on selected province

    const fetchCitiesState = () => {
      // Find the province code based on the selected province value
      const provinceCode = provincesList.find(
        (prov) => prov.value === moa.value.province
      )?.code;

      // Filter and sort selected cities based on province code
      const selectedCities = cities.data
        .filter(
          (city) =>
            city.provinceCode === provinceCode ||
            city.regionCode === provinceCode
        )
        .sort((a, b) => {
          // Sort the list alphabetically
          const nameA = a.label.toUpperCase();
          const nameB = b.label.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });

      // Find the city code based on the selected city label
      const cityCode = citiesList.find(
        (city) => city.label === moa.value.city
      )?.code;

      // Filter selected barangays based on city code
      const selectedBarangays = barangays.data
        .filter(
          (barangay) =>
            barangay.municipalityCode === cityCode ||
            barangay.cityCode === cityCode
        )
        .sort((a, b) => {
          // Sort the list alphabetically
          const nameA = a.label.toUpperCase();
          const nameB = b.label.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
      // Remove repetitive data from barangayList
      const uniqueBarangays = Array.from(
        new Set(selectedBarangays.map((barangay) => barangay.label))
      );

      // Update cityList with the labels of selected cities
      cityList.value = selectedCities.map((city) => city.label);

      // Update barangayList with the labels of selected barangays
      barangayList.value = selectedBarangays.map((barangay) => barangay.label);
    };

    const searchProvinceName = ref("");
    // Define computed property to filter provinces list based on search query
    const filteredProvincesList = computed(() => {
      const searchTerm = searchProvinceName.value.toLowerCase().trim();
      if (!searchTerm) {
        return provincesList.value;
      } else {
        const filteredProvinces = provincesList.value.filter((province) =>
          province.value.toLowerCase().includes(searchTerm)
        );
        // Check if the search query does not match any of the options
        if (filteredProvinces.length === 0) {
          // Return a dummy option indicating no matches found
          return ["No province matches found"];
        }
        return filteredProvinces;
      }
    });
    const searchCityName = ref("");
    const filteredCityList = computed(() => {
      const searchTerm = searchCityName.value.toLowerCase().trim();
      if (!searchTerm) {
        return cityList.value;
      } else {
        const filteredCities = cityList.value.filter((city) =>
          city.toLowerCase().includes(searchTerm)
        );
        // Check if the search query does not match any of the options
        if (filteredCities.length === 0) {
          // Return a dummy option indicating no matches found
          return ["No city/municipality matches found"];
        }
        return filteredCities;
      }
    });
    const searchBarangayName = ref("");
    const filteredBarangayList = computed(() => {
      const searchTerm = searchBarangayName.value.toLowerCase().trim();
      if (!searchTerm) {
        return barangayList.value;
      } else {
        const filteredBarangays = barangayList.value.filter((barangay) =>
          barangay.toLowerCase().includes(searchTerm)
        );
        // Check if the search query does not match any of the options
        if (filteredBarangays.length === 0) {
          // Return a dummy option indicating no matches found
          return ["No barangay matches found"];
        }
        return filteredBarangays;
      }
    });

    let MoaDocumentDetails = ref([]);
    let moaCount =  ref();

    const fetchAllFiles = async () => {
      try {
        const q = query(collection(db, "files"));
        const querySnapshot = await getDocs(q);
        const files = [];
        querySnapshot.forEach((doc) => {
          files.push({ id: doc.id, ...doc.data() });
        });
        return files;
      } catch (error) {
        console.error("Error fetching Transaction:", error);
        return [];
      }
    };

    // Fetch MoaDocumentDetails and update trStep
    const fetchMoa = async () => {
      try {
        const data = await fetchAllFiles();
        MoaDocumentDetails.value = data;
        
        moaCount.value = MoaDocumentDetails.value.length + 1;
        console.log(moaCount.value)
      } catch (error) {
        console.error("Error fetching MoA details:", error);
      }
    };

    onMounted(async () => {
      fetchMoa();
    });

    const onSubmit = async () => {
      try {
        moa.value.validity = `${validitycount.value} ${validitydate.value}`;

        const customMoaName = `Memorandum of Agreement_${moa.value.hteName}.pdf`;
        const newMoaUrl = await uploadFile(
          moa.value.hteMoa,
          "moa",
          customMoaName
        );

        moa.value.id = moaCount.value;
        moa.value.hteMoa = newMoaUrl;
        const docRef = await addDoc(collection(db, "files"), moa.value);

        $q.notify({
          message: "MoA upload successful!",
          caption: `${moa.value.hteName} has been uploaded.`,
          position: "top",
          type: "positive",
        });

        await router.push({
          path: "files",
        });

        console.log("MoA Upload:", data);
        saveBtnLoadingState.value = false;
      } catch (error) {
        console.error("Error uploading MoA:", error);
      }
    };

    // Function to upload a file to Firebase Storage and get its download URL
    const uploadFile = async (file, folder, customName) => {
      const fileRef = storageRef(storage, `${folder}/${customName}`);
      const snapshot = await uploadBytes(fileRef, file); // Upload the file
      const downloadURL = await getDownloadURL(snapshot.ref); // Get the file's download URL
      return downloadURL; // Return the download URL
    };

    const onReset = () => {
      moa.value = [
        {
          name: "",
          program: "",
          hteEmail: "",
          province: "",
          barangay: null,
          city: null,
          street: "",
          notaryDate: null,
          validity: null,
          document: "",
        },
      ];
    };

    return {
      moa,

      validitycount,
      validitydate,

      isInteracted,
      clearErrorMessage,
      provincesList,
      fetchCitiesState,
      barangayList,
      cityList,
      saveBtnLoadingState,
      searchProvinceName,
      filteredProvincesList,
      searchCityName,
      filteredCityList,
      searchBarangayName,
      filteredBarangayList,
      validityModel: ref(null),
      dateModel: ref(null),
      onSubmit,
      onReset,
    };
  },
};

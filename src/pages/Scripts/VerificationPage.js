import { ref, onMounted } from "vue";
import { useRouter } from "vue-router";
import { useQuasar } from "quasar";
import { getFunctions, httpsCallable } from "firebase/functions";
import { initializeApp } from "firebase/app";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const functions = getFunctions(firebaseApp);

export default {
  setup() {
    const router = useRouter();
    const email = ref(""); // Set default value to empty string
    const formcode = ref("");
    let otp = ""; // Make otp a variable, not a constant
    const $q = useQuasar();

    onMounted(() => {
      email.value = router.currentRoute.value.query.email || ""; 
      // Generate a random 4-digit OTP
      generateOTP();
      console.log("Generated OTP:", otp); 
      sendOtpToEmail();
    });

    const generateOTP = () => {
      const numbers = "0123456789";
      let code = "";
  
      for (let i = 0; i < 4; i++) {
          const randomIndex = Math.floor(Math.random() * numbers.length);
          code += numbers[randomIndex];
      }
  
      otp = code;
      console.log("Regenerated OTP:", otp); 
    };

    const sendOtpToEmail = async () => {
      const sendOtpEmail = httpsCallable(functions, 'sendOtpEmail');
      try {
        const result = await sendOtpEmail({ email: email.value, otp });
        if (result.data.success) {
          $q.notify({
            color: "positive",
            position: "top",
            message: "OTP sent successfully",
          });
        }
      } catch (error) {
        console.error("Error sending OTP email:", error);
        $q.notify({
          color: "negative",
          position: "top",
          message: "Error sending OTP",
          caption: error.message,
          icon: "report_problem",
        });
      }
    };

    const sendEmail = async () => {
      console.log(formcode.value);
      if (formcode.value === otp) {
        if (email.value.endsWith("@pup.edu.ph")) {
          router.push({ path: 'admin/dashboard' });
        }
        if (email.value.endsWith("@iskolarngbayan.pup.edu.ph")) {
          router.push({ path: 'student/files' });
        }
      } else {
        $q.notify({
          color: "negative",
          position: "top",
          message: "Incorrect OTP",
          caption: "Please try again.",
          icon: "report_problem",
        });
      }
    };

    return {
      generateOTP,
      formcode,
      email,
      sendEmail,
    };
  }
};

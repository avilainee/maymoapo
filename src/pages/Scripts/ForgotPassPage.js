import { ref } from "vue";
import { useRouter } from "vue-router";
import { useQuasar } from "quasar";
import { initializeApp } from "firebase/app"; 
import { getAuth, sendPasswordResetEmail } from "firebase/auth";
import { getFirestore, collection, addDoc, doc, updateDoc, query, where, getDocs } from "firebase/firestore";

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);
const auth = getAuth();

export default {
  setup() {
    const AccountDetails = ref([]);
    const form = ref({
      email: "",
    });
    const router = useRouter();
    const $q = useQuasar();

    const fetchAccountByEmail = async (email) => {
      try {
        const q = query(collection(db, "accounts"), where("email", "==", email));
        const querySnapshot = await getDocs(q);
        const accounts = [];
        querySnapshot.forEach((doc) => {
          accounts.push({ id: doc.id, ...doc.data() });
        });
        return accounts;
      } catch (error) {
        console.error("Error fetching account by email:", error);
        return [];
      }
    };

    const onSend = async () => {
      try {
        const data = await fetchAccountByEmail(form.value.email);
        AccountDetails.value = data;

        if (AccountDetails.value.length >= 1) {
          // Send password reset email
          await sendPasswordResetEmail(auth, form.value.email);
          
          $q.notify({
            color: "positive",
            position: "top",
            message: "Password reset email sent successfully. Check your inbox.",
            icon: "check",
          });
        } else {
          $q.notify({
            color: "negative",
            position: "top",
            message: "Email is not registered.",
            icon: "report_problem",
          });
        }
      } catch (error) {
        $q.notify({
          color: "negative",
          position: "top",
          message: "Error sending password reset email.",
          icon: "report_problem",
        });
      }
    };

    return {
      onSend,
      form,
      isPwd: ref(true),
      iscPwd: ref(true),
    };
  },
};

import { ref } from "vue";
import { useRouter } from "vue-router";
import { useQuasar } from "quasar";
import { initializeApp } from "firebase/app";
import {
  getAuth,
  createUserWithEmailAndPassword,
  updateProfile,
  sendEmailVerification,
} from "firebase/auth";
import {
  getFirestore,
  collection,
  query,
  where,
  getDocs,
  addDoc,
} from "firebase/firestore";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp);

export default {
  setup() {
    const router = useRouter();
    const $q = useQuasar();
    let password = ref("");
    let cpassword = ref("");
    let form = ref({
      id: null,
      name: "",
      stNo: null,
      email: "",
      role: "",
      program: "",
      terms: false,
    });

    const AccountDetails = ref([]);

    const fetchAccountByEmail = async (email) => {
      try {
        console.log("Fetching account for email:", email);
        const q = query(
          collection(db, "accounts"),
          where("email", "==", email)
        );
        const querySnapshot = await getDocs(q);
        const accounts = [];
        querySnapshot.forEach((doc) => {
          accounts.push({ id: doc.id, ...doc.data() });
        });
        console.log("Account by email:", accounts);
        return accounts;
      } catch (error) {
        console.error("Error fetching account by email:", error);
        return [];
      }
    };

    const AddAccount = async (account) => {
      try {
        const docRef = await addDoc(collection(db, "accounts"), account);
        console.log("Document written with ID: ", docRef.id);
      } catch (error) {
        console.error("Error adding document: ", error);
      }
    };

    const onSubmit = async () => {
      try {
        const data = await fetchAccountByEmail(form.value.email);
        AccountDetails.value = data;

        if (AccountDetails.value.length >= 1) {
          // Account already exists
          $q.notify({
            message: "Account already exists!",
            caption: `Please login.`,
            position: "top",
            timeout: 5000,
            color: "negative",
          });
          return;
        }

        if (!form.value.name || !form.value.email || !password.value) {
          // Form validation failed
          $q.notify({
            color: "negative",
            position: "top",
            message: "Please complete the form.",
            icon: "report_problem",
          });
          return;
        }

        // Get the Firebase auth instance
        const auth = getAuth(firebaseApp);

        // Register the user with Firebase Authentication
        await createUserWithEmailAndPassword(
          auth,
          form.value.email,
          password.value
        )
          .then(async (userCredential) => {
            // Set the displayName after successful registration
            await updateProfile(userCredential.user, {
              displayName: form.value.name,
            });

            if (form.value.email.endsWith("@pup.edu.ph")) {
              // Add account to Firestore
              await AddAccount(form.value);

              $q.notify({
                message: "Account created successfully!",
                caption: `${form.value.name} has been created.`,
                position: "top",
                color: "positive",
              });

              router.push({ path: "/login" });
            }
            if (form.value.email.endsWith("@iskolarngbayan.pup.edu.ph")) {
              // Send email verification
              await sendEmailVerification(auth.currentUser);

              $q.notify({
                message: "Verification email sent successfully.",
                position: "top",
                color: "positive",
              });

              // Add account to Firestore
              await AddAccount(form.value);

              $q.notify({
                message: "Account created successfully!",
                caption: `${form.value.name} has been created.`,
                position: "top",
                color: "positive",
              });

              router.push({ path: "/login" });
            }
          })
          .catch((error) => {
            console.error("Error registering user:", error);
            $q.notify({
              color: "negative",
              position: "top",
              message: "Error creating account.",
              icon: "report_problem",
            });
          });
      } catch (error) {
        console.error("Error creating account:", error);
      }
    };

    return {
      form,
      password,
      cpassword,
      onSubmit,
      AddAccount,
      isPwd: ref(true),
      iscPwd: ref(true),
      termsPrompt: ref(false),
      showPasswordError: ref(false),
    };
  },
  computed: {
    showRole() {
      return this.form.email.endsWith("@pup.edu.ph");
    },
    showProgram() {
      return this.form.email.endsWith("@iskolarngbayan.pup.edu.ph");
    },
    passwordsMatch() {
      return this.password === this.cpassword;
    },
    showMismatchError() {
      return this.password && this.cpassword && !this.passwordsMatch;
    },
    showEmailError() {
      return this.form.email && !this.isValidEmail;
    },
    isValidEmail() {
      const validDomains = ["pup.edu.ph", "iskolarngbayan.pup.edu.ph"];
      const domain = this.form.email.split("@")[1];
      return validDomains.includes(domain);
    },
  },
  methods: {
    validateEmail() {
      this.form.email = this.form.email.trim(); // Trim leading/trailing spaces
    },
  },
};

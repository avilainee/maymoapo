import { ref } from "vue";
import { useRouter } from "vue-router";
import { useQuasar } from "quasar";
import { initializeApp } from "firebase/app";
import {
  getAuth,
  signInWithEmailAndPassword,
  sendPasswordResetEmail,
  sendEmailVerification,
} from "firebase/auth";
import {
  getFirestore,
  collection,
  query,
  where,
  getDocs,
} from "firebase/firestore";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
// Initialize Cloud Firestore and get a reference to the service
const db = getFirestore(firebaseApp);
// Get the Firebase auth instance
const auth = getAuth(firebaseApp);

export default {
  setup() {
    const AccountDetails = ref([]);
    const form = ref({
      email: "",
      password: "",
    });
    const isPwd = ref(true);
    const router = useRouter();
    const $q = useQuasar();

    const fetchAccountByEmail = async (email) => {
      try {
        console.log("Fetching account for email:", email);
        const q = query(
          collection(db, "accounts"),
          where("email", "==", email)
        );
        const querySnapshot = await getDocs(q);
        const accounts = [];
        querySnapshot.forEach((doc) => {
          accounts.push({ id: doc.id, ...doc.data() });
        });
        console.log("Account by email:", accounts);
        return accounts;
      } catch (error) {
        console.error("Error fetching account by email:", error);
        return [];
      }
    };

    const onSignIn = async () => {
      try {
        // Fetch account details by email
        const data = await fetchAccountByEmail(form.value.email);
        AccountDetails.value = data;
        console.log("Fetched Account Details:", AccountDetails.value);

        // If account exists and password matches
        if (AccountDetails.value.length >= 1) {
          // Sign in the user with Firebase Authentication
          await signInWithEmailAndPassword(
            auth,
            form.value.email,
            form.value.password
          )
            .then(() => {
              console.log("Successfully logged in with Firebase!");
              console.log(auth.currentUser);

              if (form.value.email.endsWith("@pup.edu.ph")) {
                router.push({ path: "admin/dashboard" });
              }

              else if (form.value.email.endsWith("@iskolarngbayan.pup.edu.ph")) {
                const user = auth.currentUser; // Check if email is verified
                if (!user.emailVerified) {
                  // Send email verification
                  sendEmailVerification(auth.currentUser);
                  $q.notify({
                    color: "negative",
                    position: "top",
                    message: "Please verify your email address.",
                    caption: `Verification email sent successfully.`,
                    icon: "report_problem",
                  });
                  return;
                } else {
                  if (form.value.email.endsWith("@pup.edu.ph")) {
                    router.push({ path: "admin/dashboard" });
                  }
                  if (form.value.email.endsWith("@iskolarngbayan.pup.edu.ph")) {
                    router.push({ path: "student/files" });
                  }
                }
              }
            })
            .catch((error) => {
              console.log(error.code);
              $q.notify({
                color: "negative",
                position: "top",
                message: "Email and Password don't match.",
                icon: "report_problem",
              });
            });
        } else if (AccountDetails.value.length === 0) {
          $q.notify({
            color: "negative",
            position: "top",
            message: "Email is not registered.",
            icon: "report_problem",
          });
        } else {
          $q.notify({
            color: "negative",
            position: "top",
            message: "Email and Password don't match.",
            icon: "report_problem",
          });
        }
      } catch (error) {
        console.error("Error during sign-in:", error);
        $q.notify({
          color: "negative",
          position: "top",
          message: "Enter your credentials.",
          icon: "report_problem",
        });
      }
    };

    const onForgotPassword = async () => {
      try {
        if (form.value.email) {
          await sendPasswordResetEmail(auth, form.value.email);
          $q.notify({
            color: "positive",
            position: "top",
            message: "Password reset email sent successfully.",
            icon: "check",
          });
        } else {
          $q.notify({
            color: "negative",
            position: "top",
            message: "Please enter your email.",
            icon: "report_problem",
          });
        }
      } catch (error) {
        console.error("Error sending password reset email:", error);
        $q.notify({
          color: "negative",
          position: "top",
          message: "Error sending password reset email.",
          icon: "report_problem",
        });
      }
    };

    return {
      onForgotPassword,
      onSignIn,
      AccountDetails,
      form,
      isPwd,
    };
  },
};

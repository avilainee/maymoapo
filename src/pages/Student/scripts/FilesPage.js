import { ref, onMounted, computed } from "vue";
import { useRoute, useRouter } from "vue-router";
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import {
  getFirestore,
  query,
  collection,
  where,
  getDocs,
} from "firebase/firestore";
import {
  getStorage,
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from "firebase/storage";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp); // Initialize Cloud Firestore and get a reference to the service
const storage = getStorage(firebaseApp); // Initialize Firebase Storage

export default {
  methods: {
    clearSearch() {
      // Clear the search filter
      this.filter = "";
      // Trigger the button action if needed
      if (this.button && this.button.click) {
        this.button.click();
      }
    },
  },
  setup() {
    const router = useRouter();
    const route = useRoute();
    let notaryDateFilter = ref("All time");
    let customStartDate = ref(null);
    let customEndDate = ref(null);
    const userTransactionCount = ref();

    const notaryDateOptions = [
      "All time",
      "Today",
      "Yesterday",
      "Past 7 days",
      "Past 30 days",
      "Custom Range",
    ];
    let MoaDocumentDetails = ref([]);
    let trNo = ref();
    let trStep = ref();
    let moaCount =  ref();

    const fetchAllFiles = async () => {
      try {
        const q = query(collection(db, "files"));
        const querySnapshot = await getDocs(q);
        const files = [];
        querySnapshot.forEach((doc) => {
          files.push({ id: doc.id, ...doc.data() });
        });
        return files;
      } catch (error) {
        console.error("Error fetching Transaction:", error);
        return [];
      }
    };

    // Fetch MoaDocumentDetails and update trStep
    const fetchMoa = async () => {
      try {
        const data = await fetchAllFiles();
        MoaDocumentDetails.value = data;
        
        moaCount.value = MoaDocumentDetails.value.length + 1;
        console.log(moaCount.value)
      } catch (error) {
        console.error("Error fetching MoA details:", error);
      }
    };

    onMounted(async () => {
      fetchMoa();

      const auth = getAuth(firebaseApp);
      const user = auth.currentUser;
      const AccountDetails = ref([]);

      if (user) {
        const data = await fetchTransactionByEmail(user.email);
        AccountDetails.value = data;
        trNo.value = AccountDetails.value[0].trNo;
        trStep.value = AccountDetails.value[0].trStep;
        userTransactionCount.value = AccountDetails.value.length;
      }
    });

    const fetchTransactionByEmail = async (email) => {
      try {
        console.log("Fetching Transaction for email:", email);
        const q = query(
          collection(db, "transactions"),
          where("stEmail", "==", email)
        );
        const querySnapshot = await getDocs(q);
        const transactions = [];
        querySnapshot.forEach((doc) => {
          transactions.push({ id: doc.id, ...doc.data() });
        });
        console.log("Transaction by email:", transactions);
        return transactions;
      } catch (error) {
        console.error("Error fetching Transaction by email:", error);
        return [];
      }
    };

    const rows = [{}];

    // Define columns
    const columns = [
      {
        name: "index",
        label: "#",
        field: "id",
      },
      {
        name: "hteName",
        required: true,
        label: "HTE/Company Name",
        align: "left",
        field: "hteName",
        sortable: true,
      },
      {
        required: true,
        label: "Address",
        align: "left",
        field: "province", // Sort by province
        format: (val, row) => `${row.barangay}, ${row.city}, ${row.province}`,
        sortable: true,
      },
      {
        name: "hteEmail",
        required: true,
        label: "Email",
        align: "left",
        field: "hteEmail",
      },
      {
        name: "notaryDate",
        required: true,
        label: "Notary",
        align: "left",
        field: "notaryDate",
        sortable: true,
      },
      {
        name: "actions",
        align: "center",
        field: "actions",
      },
    ];

    // Computed property for filtered MoaDocumentDetails
    const filteredMoaDocumentDetails = computed(() => {
      // Filter by notary date
      if (notaryDateFilter.value === "Today") {
        // Filter documents with notary date equal to today's date
        const today = new Date();
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          return (
            notaryDate.getFullYear() === today.getFullYear() &&
            notaryDate.getMonth() === today.getMonth() &&
            notaryDate.getDate() === today.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Yesterday") {
        // Filter documents with notary date equal to yesterday's date
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1); // Set date to yesterday
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          return (
            notaryDate.getFullYear() === yesterday.getFullYear() &&
            notaryDate.getMonth() === yesterday.getMonth() &&
            notaryDate.getDate() === yesterday.getDate()
          );
        });
      } else if (notaryDateFilter.value === "Past 7 days") {
        // Filter documents with notary date within the past 7 days
        const today = new Date();
        const sevenDaysAgo = new Date(today);
        sevenDaysAgo.setDate(today.getDate() - 7); // Set date to 7 days ago
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          return notaryDate >= sevenDaysAgo && notaryDate <= today;
        });
      } else if (notaryDateFilter.value === "Past 30 days") {
        // Filter documents with notary date within the past 30 days
        const today = new Date();
        const thirtyDaysAgo = new Date(today);
        thirtyDaysAgo.setDate(today.getDate() - 30);
        console.log(thirtyDaysAgo);
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          return notaryDate >= thirtyDaysAgo && notaryDate <= today;
        });
      } else if (customStartDate.value == null && customEndDate.value == null) {
        notaryDateFilter.value === "Custom Range";
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return MoaDocumentDetails.value;
        });
      } else if (customStartDate.value != null && customEndDate.value == null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return notaryDate >= startDay;
        });
      } else if (customStartDate.value == null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return notaryDate <= endDay;
        });
      } else if (customStartDate.value != null && customEndDate.value != null) {
        return MoaDocumentDetails.value.filter((doc) => {
          const notaryDate = new Date(doc.notaryDate);
          const startDay = new Date(customStartDate.value);
          const endDay = new Date(customEndDate.value);
          return notaryDate >= startDay && notaryDate <= endDay;
        });
      } else {
        return MoaDocumentDetails.value; // Return all documents if no specific filter is selected
      }
    });

    const onPreviewMoa = async (row) => {
      window.open(row.hteMoa, "_blank");
    };

    const onProcessNewMoa = () => {
      if (userTransactionCount.value >= 1) {
        if (trStep.value >= 2.0) {
          router.push({
            path: "track-moa",
            query: {
              trNo: trNo.value,
            },
          });
        } else if (trStep.value >= 1.1 && trStep.value <= 1.3) {
          router.push({
            path: "generate-moa",
            query: {
              trNo: trNo.value,
            },
          });
        }
      } else {
        router.push({
          path: "generate-moa",
        });
      }
    };
    return {
      filteredMoaDocumentDetails,
      notaryDateOptions,
      notaryDateFilter,
      customStartDate,
      customEndDate,
      filter: ref(""),
      MoaDocumentDetails,
      onPreviewMoa,
      columns,
      rows,
      userTransactionCount,
      trNo,
      trStep,
      onProcessNewMoa,
    };
  },
  data() {
    return {
      visible: true,
    };
  },
  mounted() {
    setTimeout(() => {
      this.visible = false;
    }, 1000);
  },
};

import { ref, onMounted, computed } from "vue";
import { useQuasar } from "quasar";
import barangays from "../../Servers/barangays.json";
import cities from "../../Servers/cities.json";
import provinces from "../../Servers/provinces.json";
import { useRouter, useRoute } from "vue-router";
import { initializeApp } from "firebase/app";
import {
  getAuth,
  reauthenticateWithCredential,
  updatePassword,
  EmailAuthProvider,
} from "firebase/auth"; // Import EmailAuthProvider
import {
  getFirestore,
  collection,
  doc,
  where,
  query,
  getDocs,
  getDoc,
  addDoc,
  updateDoc,
  deleteDoc,
} from "firebase/firestore";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const db = getFirestore(firebaseApp); // Initialize Cloud Firestore and get a reference to the service

const auth = getAuth(firebaseApp);
const user = auth.currentUser;

export default {
  name: "CustomizedMoa",
  setup() {
    const $q = useQuasar();
    const router = useRouter();
    const route = useRoute();

    const saveBtnLoadingState = ref(false);
    const provincesList = provinces.data;
    const citiesList = cities.data;
    const cityList = ref([]);
    const barangayList = ref([]);

    const MoaDocumentDetails = ref([]);
    const moa = ref({
      id: null,
      trNo: null,
      trStep: 1.2,
      signatoryName: "",
      signatoryPosition: "",
      hteName: "",
      hteClass: "",
      hteEmail: "",
      htePhone: null,
      province: null,
      barangay: null,
      city: null,
      street: "",
      notaryDate: null,
      validity: null,
      hteMoa: null,
      hteDti: null,
      hteSec: null,
    });

    // Define reactive reference to track if moa fields are interacted with
    const isInteracted = ref({
      province: false,
      barangay: false,
      city: false,
    });

    // Function to mark field as interacted
    const clearErrorMessage = (fieldName) => {
      isInteracted.value[fieldName] = true;
    };

    const fetchTransactionByEmail = async (email) => {
      try {
        console.log("Fetching Transaction for email:", email);
        const q = query(collection(db, "transactions"), where("stEmail", "==", email));
        const querySnapshot = await getDocs(q);
        const transactions = [];
        querySnapshot.forEach((doc) => {
          transactions.push({ id: doc.id, ...doc.data() });
        });
        console.log("Transaction by emailzz:", transactions);
        return transactions;
      } catch (error) {
        console.error("Error fetching Transaction by email:", error);
        return [];
      }
    };

    const fetchMoa = async () => {
      try {
        if (route.query.trNo) {
          const q = query(collection(db, "transactions"), where("trNo", "==", route.query.trNo));
          const querySnapshot = await getDocs(q);

          if (!querySnapshot.empty) {
            const doc = querySnapshot.docs[0];
            const email = doc.data().stEmail;
            const transactions = await fetchTransactionByEmail(email);
            
            if (transactions.length > 0) {
              const firstTransaction = transactions[0];
              MoaDocumentDetails.value = firstTransaction;
              moa.value = {
                id: querySnapshot.docs[0].id,
                hteName: firstTransaction.hteName,
                hteClass: firstTransaction.hteClass,
                signatoryName: firstTransaction.signatoryName,
                signatoryPosition: firstTransaction.signatoryPosition,
                hteEmail: firstTransaction.hteEmail,
                htePhone: firstTransaction.htePhone,
                province: firstTransaction.province,
                city: firstTransaction.city,
                barangay: firstTransaction.barangay,
                street: firstTransaction.street
              };
            } else {
              console.log("No transactions found for the given email.");
            }
          } else {
            console.log("No such document!");
          }
        } else {
          console.error("Error: route.query.trNo is undefined or null.");
        }
      } catch (error) {
        console.error("Error fetching MoA:", error);
      }
    };

    const addMoa = async () => {
      try {
        if (
          moa.value.signatoryName &&
          moa.value.signatoryPosition &&
          moa.value.hteName &&
          moa.value.hteClass &&
          moa.value.hteEmail &&
          moa.value.htePhone &&
          moa.value.province &&
          moa.value.barangay
        ) {
          moa.value.stEmail = user.email;
          moa.value.trNo = generateTransactionNumber(10);

          const docRef = await addDoc(collection(db, "transactions"), moa.value);

          await router.push({
            path: "generate-moa",
            query: {
              trNo: moa.value.trNo,
            },
          });

          exportAsDoc();
          $q.notify({
            message: "MoA downloaded successfully!",
            caption: `${moa.value.hteName} transaction has been started.`,
            position: "top",
            type: "positive",
          });

          await fetchMoa();  // Reload the MoA details after addition
        } else {
          $q.notify({
            color: "negative",
            position: "top",
            message: "Complete the form to continue.",
            icon: "report_problem",
          });
        }
      } catch (error) {
        console.error("Error uploading MoA:", error);
      }
    };

    const updateMoa = async () => {
      try {
        if (!route.query.trNo) {
          console.error("Error: Transaction Number (trNo) is null or undefined.");
          return;
        }
    
        // Fetch the document using trNo
        const q = query(collection(db, "transactions"), where("trNo", "==", route.query.trNo));
        const querySnapshot = await getDocs(q);
    
        if (!querySnapshot.empty) {
          const doc = querySnapshot.docs[0]; // Assuming trNo is unique and there is only one document
          const docRef = doc.ref;
    
          console.log("Updating document with trNo:", route.query.trNo);
    
          await updateDoc(docRef, {
            trStep: 1.2,
            hteName: moa.value.hteName,
            hteClass: moa.value.hteClass,
            signatoryName: moa.value.signatoryName,
            signatoryPosition: moa.value.signatoryPosition,
            hteEmail: moa.value.hteEmail,
            htePhone: moa.value.htePhone,
            province: moa.value.province,
            city: moa.value.city,
            barangay: moa.value.barangay,
            street: moa.value.street,
          });
    
          await router.push({
            path: "generate-moa",
            query: {
              trNo: route.query.trNo,
            },
          });
    
          exportAsDoc();
    
          $q.notify({
            message: "MoA downloaded successfully!",
            caption: `${moa.value.hteName} transaction has been updated.`,
            position: "top",
            type: "positive",
          });
    
        } else {
          console.log("No document found with trNo:", route.query.trNo);
          $q.notify({
            color: "negative",
            position: "top",
            message: `No document found with transaction number: ${route.query.trNo}`,
            icon: "report_problem",
          });
        }
      } catch (error) {
        console.error("Error updating MoA:", error);
        $q.notify({
          color: "negative",
          position: "top",
          message: "Error updating MoA. Please try again later.",
          icon: "report_problem",
        });
      }
    };

    const onProceed = async () => {
      closeDialog();

      const onRefresh = () => {
        console.log("hellu");
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      };

      if (!route.query.trNo) {
        await addMoa();
        onRefresh();
      } else {
        console.log("test")
        await updateMoa();
        onRefresh();
      }
    };

    onMounted(() => {
      fetchMoa();
    });

    const deleteMoa = async () => {
      try {
        if (!route.query.trNo) {
          console.error("Error: Transaction Number (trNo) is null or undefined.");
          return;
        }
    
        // Fetch the document using trNo
        const q = query(collection(db, "transactions"), where("trNo", "==", route.query.trNo));
        const querySnapshot = await getDocs(q);
    
        if (!querySnapshot.empty) {
          const doc = querySnapshot.docs[0]; // Assuming trNo is unique and there is only one document
          const docRef = doc.ref;
    
          console.log("Deleting document with trNo:", route.query.trNo);
    
          await deleteDoc(docRef);
    
          await router.push({
            path: "files",
          });
    
          $q.notify({
            message: "MoA deleted successfully!",
            position: "top",
            type: "positive",
          });
    
        } else {
          console.log("No document found with trNo:", route.query.trNo);
          $q.notify({
            color: "negative",
            position: "top",
            message: `No document found with transaction number: ${route.query.trNo}`,
            icon: "report_problem",
          });
        }
      } catch (error) {
        console.error("Error deleting MoA:", error);
        $q.notify({
          color: "negative",
          position: "top",
          message: "Error deleting MoA. Please try again later.",
          icon: "report_problem",
        });
      }
    };
    

    const generateTransactionNumber = (length) => {
      const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
      let trNo = "";
      for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * characters.length);
        trNo += characters[randomIndex];
      }
      return trNo;
    };

    const exportAsDoc = () => {
      var html =
        "<html xmlns:o='urn:schemas-microsoft-com:office:office' xmlns:w='urn:schemas-microsoft-com:office:word' xmlns='http://www.w3.org/TR/REC-html40'>" +
        "<head><meta charset='utf-8'><title>Export HTML To Doc</title>" +
        "<style>" +
        "@page { size: A4; margin: 1in; }" +
        "div.Section1 { page: Section1; }" +
        "body { font-family: Arial, sans-serif; text-align: justify; font-size: 11pt; }" +
        "ol { padding-left: 0px; }" +
        "</style>" +
        "</head><body><div class='Section1'>";

      var footer = "</div></body></html>";
      html += document.getElementById("exportthis").innerHTML + footer;

      var url = "data:application/vnd.ms-word;charset=utf-8," + encodeURIComponent(html);

      var filename = "Memorandum_of_Agreement-document.doc";

      var downloadLink = document.createElement("a");
      document.body.appendChild(downloadLink);
      downloadLink.href = url;
      downloadLink.download = filename;
      downloadLink.click();
      document.body.removeChild(downloadLink);
    };

    const showDialog = ref(false);

    const openDialog = () => {
      showDialog.value = true;
    };

    const closeDialog = () => {
      showDialog.value = false;
    };

    const onSubmit = () => {
      if (
        moa.value.signatoryName &&
        moa.value.signatoryPosition &&
        moa.value.hteName &&
        moa.value.hteClass &&
        moa.value.hteEmail &&
        moa.value.htePhone &&
        moa.value.province &&
        moa.value.barangay
      ) {
        openDialog();
      } else {
        $q.notify({
          color: "negative",
          position: "top",
          message: "Complete the form to continue.",
          icon: "report_problem",
        });
      }
    };

    const onCancel = () => {
      deleteMoa();
    };

    // Sorting provinces list alphabetically
    provincesList.value = provinces.data.sort((a, b) => {
      // Sort the list alphabetical
      const nameA = a.label.toUpperCase();
      const nameB = b.label.toUpperCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });
    // Function to fetch cities and barangays based on selected province

    const fetchCitiesState = () => {
      // Find the province code based on the selected province value
      const provinceCode = provincesList.find(
        (prov) => prov.value === moa.value.province
      )?.code;

      // Filter and sort selected cities based on province code
      const selectedCities = cities.data
        .filter(
          (city) =>
            city.provinceCode === provinceCode ||
            city.regionCode === provinceCode
        )
        .sort((a, b) => {
          // Sort the list alphabetically
          const nameA = a.label.toUpperCase();
          const nameB = b.label.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });

      // Find the city code based on the selected city label
      const cityCode = citiesList.find(
        (city) => city.label === moa.value.city
      )?.code;

      // Filter selected barangays based on city code
      const selectedBarangays = barangays.data
        .filter(
          (barangay) =>
            barangay.municipalityCode === cityCode ||
            barangay.cityCode === cityCode
        )
        .sort((a, b) => {
          // Sort the list alphabetically
          const nameA = a.label.toUpperCase();
          const nameB = b.label.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
      // Remove repetitive data from barangayList
      const uniqueBarangays = Array.from(
        new Set(selectedBarangays.map((barangay) => barangay.label))
      );

      // Update cityList with the labels of selected cities
      cityList.value = selectedCities.map((city) => city.label);

      // Update barangayList with the labels of selected barangays
      barangayList.value = selectedBarangays.map((barangay) => barangay.label);
    };

    const searchProvinceName = ref("");
    // Define computed property to filter provinces list based on search query
    const filteredProvincesList = computed(() => {
      const searchTerm = searchProvinceName.value.toLowerCase().trim();
      if (!searchTerm) {
        return provincesList.value;
      } else {
        const filteredProvinces = provincesList.value.filter((province) =>
          province.value.toLowerCase().includes(searchTerm)
        );
        // Check if the search query does not match any of the options
        if (filteredProvinces.length === 0) {
          // Return a dummy option indicating no matches found
          return ["No province matches found"];
        }
        return filteredProvinces;
      }
    });
    const searchCityName = ref("");
    const filteredCityList = computed(() => {
      const searchTerm = searchCityName.value.toLowerCase().trim();
      if (!searchTerm) {
        return cityList.value;
      } else {
        const filteredCities = cityList.value.filter((city) =>
          city.toLowerCase().includes(searchTerm)
        );
        // Check if the search query does not match any of the options
        if (filteredCities.length === 0) {
          // Return a dummy option indicating no matches found
          return ["No city/municipality matches found"];
        }
        return filteredCities;
      }
    });
    const searchBarangayName = ref("");
    const filteredBarangayList = computed(() => {
      const searchTerm = searchBarangayName.value.toLowerCase().trim();
      if (!searchTerm) {
        return barangayList.value;
      } else {
        const filteredBarangays = barangayList.value.filter((barangay) =>
          barangay.toLowerCase().includes(searchTerm)
        );
        // Check if the search query does not match any of the options
        if (filteredBarangays.length === 0) {
          // Return a dummy option indicating no matches found
          return ["No barangay matches found"];
        }
        return filteredBarangays;
      }
    });

    return {
      moa,
      isInteracted,
      clearErrorMessage,
      provincesList,
      fetchCitiesState,
      barangayList,
      cityList,
      saveBtnLoadingState,
      searchProvinceName,
      filteredProvincesList,
      searchCityName,
      filteredCityList,
      searchBarangayName,
      filteredBarangayList,
      MoaDocumentDetails,
      onSubmit,
      onProceed,
      onCancel,
      showDialog,
      openDialog,
      closeDialog,
    };
  },
};

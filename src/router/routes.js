import { initializeApp } from "firebase/app"; // Import initializeApp from firebase/app
import { getAuth, onAuthStateChanged, signOut } from "firebase/auth";

// Your Firebase configuration object
const firebaseConfig = {
  apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
  authDomain: "maymoapo-815ee.firebaseapp.com",
  projectId: "maymoapo-815ee",
  storageBucket: "maymoapo-815ee.appspot.com",
  messagingSenderId: "992963522084",
  appId: "1:992963522084:web:f79e171bfb56a05359f2b1",
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);

const auth = getAuth(firebaseApp);

// Function to check if the user is authenticated
const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      unsubscribe(); // Unsubscribe to prevent memory leaks
      resolve(user); // Resolve with the current user if available
    }, reject); // Reject with any error
  });
};

// Function to check if the user is authenticated
const requireAuth = (to, from, next) => {
  getCurrentUser().then((user) => {
    if (!user) {
      next({ path: '/login' }); // Redirect to login if not authenticated
    } else {
      next(); // Proceed to the requested route
    }
  }).catch((error) => {
    console.error("Error getting current user:", error);
    next({ path: '/login' }); // Redirect to login if there's an error
  });
};

const routes = [
  {
    path: "/login",
    component: () => import("../pages/LoginPage.vue"),
  },
  {
    path: "/registration",
    component: () => import("../pages/RegistrationPage.vue"),
  },
  {
    path: "/forgot-password",
    component: () => import("../pages/ForgotPassPage.vue"),
  },
  {
    path: "/verify",
    component: () => import("../pages/VerificationPage.vue"),
  },
  {
    path: '/admin',
    component: () => import('layouts/MainLayoutAdmin.vue'),
    beforeEnter: requireAuth,
    children: [
      {
        path: 'dashboard',
        name: 'dashboard',
        component: () => import('pages/Admin/DashboardPage.vue'),
        beforeEnter: requireAuth,
      },
      {
        path: 'notification',
        name: 'notification-admin',
        component: () => import('pages/Admin/NotificationPage.vue'),
        beforeEnter: requireAuth,
      },
      {
        path: 'transaction',
        name: 'transaction',
        component: () => import('pages/Admin/TransactionPage.vue'),
        beforeEnter: requireAuth,
      },
      {
        path: 'files',
        name: 'files-admin',
        component: () => import('pages/Admin/FilesPage.vue'),
        beforeEnter: requireAuth,
      },
      {
        path: "upload-moa",
        name: "upload-moa",
        component: () => import("../pages/Admin/UploadMoa.vue"),
        beforeEnter: requireAuth,
      },
      {
        path: "account-settings",
        name: "account-settings-admin",
        component: () => import("../pages/Admin/SettingsPage.vue"),
        beforeEnter: requireAuth,
      },
      {
        path: "terms-and-agreement",
        name: "terms-and-agreement-admin",
        component: () => import("../pages/TermsPage.vue"),
        beforeEnter: requireAuth,
      },
      {
        path: "index",
        name: "index",
        component: () => import("../pages/IndexPage.vue"),
        beforeEnter: requireAuth,
      },
    ],
  },
  {
    path: '/student',
    component: () => import('layouts/MainLayoutStudent.vue'),
    beforeEnter: requireAuth,
    children: [
      {
        path: 'files',
        name: 'files',
        component: () => import('pages/Student/FilesPage.vue'),
        beforeEnter: requireAuth,
      },
      {
        path: 'generate-moa',
        name: 'generate-moa',
        component: () => import('pages/Student/GenerateMoaPage.vue'),
        beforeEnter: requireAuth,
      },
      {
        path: 'track-moa',
        name: 'track-moa',
        component: () => import('pages/Student/TrackingPage.vue'),
        beforeEnter: requireAuth,
      },
      {
        path: 'notification',
        name: 'notification',
        component: () => import('pages/Student/NotificationPage.vue'),
        beforeEnter: requireAuth,
      },
      {
        path: "account-settings",
        name: "account-settings",
        component: () => import("../pages/Student/SettingsPage.vue"),
        beforeEnter: requireAuth,
      },
    ],
  },
  {
    path: '/moa',
    component: () => import('pages/Student/components/CustomizedMoa.vue')
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import("../pages/LoginPage.vue")
  }
]

export default routes

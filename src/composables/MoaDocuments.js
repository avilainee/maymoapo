import { httpGet, httpPut, httpPost, httpDel } from "src/boot/axios";
import { ref, readonly } from "vue";

let SetMoaDocument = ref([]);
let GetMoaDocuments = readonly(SetMoaDocument);

const FetchMoaDocuments = () => {
  return new Promise((resolve, reject) => {
    httpGet("MoaDocuments", {
      success(response) {
        response.data.status === "success" &&
          (SetMoaDocument.value = response.data.data);

        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

const FetchMoaDocumentDetails = (trNo) => {
  return new Promise((resolve, reject) => {
    httpGet(`MoaDocuments/?trNo=${trNo}`, {
      success(response) {
        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

const AddMoa = (payload) => {
  return new Promise((resolve, reject) => {
    httpPost("MoaDocuments", payload, {
      success(response) {
        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

const UpdateMoa = (payload) => {
  return new Promise((resolve, reject) => {
    httpPut(`MoaDocuments/${payload.id}`, payload, {
      success(response) {
        resolve(response.data);
      },
      catch(error) {
        console.error("Error updating MoA document:", error);
        reject(error);
      },
    });
  });
};

const DeleteMoa = (payload) => {
	return new Promise((resolve, reject) => {
		httpDel(`MoaDocuments/${payload.id}`, payload, {
			success(response) {
				resolve(response.data);
			},
			catch(error) {
				console.error('Error MoA document', error);
				reject(error);
			},
		});
	});
};


const FetchAccountDetails = (email) => {
  return new Promise((resolve, reject) => {
    httpGet(`Accounts/?email=${email}`, {
      success(response) {
        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

const AddAccount = (payload) => {
  return new Promise((resolve, reject) => {
    httpPost("Accounts", payload, {
      success(response) {
        resolve(response.data);
      },
      catch(response) {
        reject(response);
      },
    });
  });
};

// Update retail store details by trNo
const UpdateAccount = (payload) => {
  return new Promise((resolve, reject) => {
    httpPut(`Accounts/${payload.id}`, payload, {
      success(response) {
        resolve(response.data);
      },
      catch(error) {
        console.error("Error updating MoA document:", error);
        reject(error);
      },
    });
  });
};

export {
  AddMoa,
  UpdateMoa,
  DeleteMoa,
  GetMoaDocuments,
  FetchMoaDocuments,
  FetchMoaDocumentDetails,
  FetchAccountDetails,
  AddAccount,
  UpdateAccount,
};

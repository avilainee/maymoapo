var express = require('express');
var http = require('http');
var path = require('path');
var nodemailer = require('nodemailer');
var cors = require('cors'); // Importing cors

var app = express();
var server = http.Server(app);
var port = 5000;

app.use(cors()); // Enabling CORS for all requests
app.set("port", port);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "page")));

// Routing
app.get("/", function(req, res) {
    res.sendFile(path.join(__dirname, "page/index.html")); // Adjust to your entry point for Vue app
});

app.post("/send-email", async function(req, res) {
    var { from, to, subject, message } = req.body;

    let transporter = nodemailer.createTransport({
        service: "hotmail",
        auth: {
            user: "mavimbang@iskolarngbayan.pup.edu.ph",
            pass: "#songha131",
        },
    });

    var mailOption = {
        from: from || "mavimbang@iskolarngbayan.pup.edu.ph", // Default from email
        to: to,
        subject: subject,
        text: message,
    };

    try {
        let info = await transporter.sendMail(mailOption);
        console.log("Email sent: " + info.response);
        res.status(200).json({ messageId: info.messageId });
    } catch (error) {
        console.error("Error sending email: ", error);
        res.status(500).json({ error: "Failed to send email", details: error.message });
    }
});

server.listen(port, function() {
    console.log("Server running on port " + port);
});

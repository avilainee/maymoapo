import firebase from 'firebase/app';
import 'firebase/auth'; // Import the Auth module

const firebaseConfig = {
    apiKey: "AIzaSyCaLXiK7TyxB2_cp68_SxUDj7KxOy2XZAk",
    authDomain: "maymoapo-815ee.firebaseapp.com",
    projectId: "maymoapo-815ee",
    storageBucket: "maymoapo-815ee.appspot.com",
    messagingSenderId: "992963522084",
    appId: "1:992963522084:web:f79e171bfb56a05359f2b1"
};

export default ({ app }) => {
  firebase.initializeApp(firebaseConfig);
};

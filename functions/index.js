/**
 * Import function triggers from their respective submodules:
 *
 * const {onCall} = require("firebase-functions/v2/https");
 * const {onDocumentWritten} = require("firebase-functions/v2/firestore");
 *
 * See a full list of supported triggers at https://firebase.google.com/docs/functions
 */

const {onRequest} = require("firebase-functions/v2/https");
const logger = require("firebase-functions/logger");

// Create and deploy your first functions
// https://firebase.google.com/docs/functions/get-started

// exports.helloWorld = onRequest((request, response) => {
//   logger.info("Hello logs!", {structuredData: true});
//   response.send("Hello from Firebase!");
// });

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const cors = require('cors')({ origin: true });
const admin = require('firebase-admin');

admin.initializeApp();

exports.downloadMoa = functions.https.onRequest((req, res) => {
  cors(req, res, async () => {
    const hteName = req.query.hteName;
    const filePath = `moa/Memorandum of Agreement_${hteName}.pdf`;
    const bucket = admin.storage().bucket();

    try {
      const file = bucket.file(filePath);
      const [metadata] = await file.getMetadata();
      const fileStream = file.createReadStream();

      res.setHeader('Content-Type', metadata.contentType);
      res.setHeader('Content-Disposition', `attachment; filename="${file.name}"`);

      fileStream.pipe(res).on('error', (err) => {
        console.error('Error downloading file:', err);
        res.status(500).send('Error downloading file');
      });
    } catch (error) {
      console.error('Error fetching file:', error);
      res.status(500).send('Error fetching file');
    }
  });
});

// Configure the email transporter using nodemailer
const transporter = nodemailer.createTransport({
  service: 'gmail', // You can use any email service
  auth: {
    user: 'maymoapo@gmail.com',
    pass: 'maymoapo2024',
  },
});

// Cloud Function to send OTP email
exports.sendOtpEmail = functions.https.onRequest((req, res) => {
  cors(req, res, () => {
    const { email, otp } = req.body;

    const mailOptions = {
      from: 'maymoapo@gmail.com',
      to: email,
      subject: 'Your OTP Code',
      text: `Your OTP code is ${otp}`,
    };

    transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        console.log('Error sending email:', error);
        return res.status(500).send({ error: 'Unable to send email' });
      } else {
        console.log('Email sent:', info.response);
        return res.status(200).send({ success: true });
      }
    });
  });
});
